// js


$(document).ready(function () {
	'use strict';
	$('.header-search a').click(function () {
		$('.search-box').fadeIn();
		$('body').css('overflow', 'hidden');
	});

	$('.close-search').click(function () {
		$('.search-box').fadeOut();
		$('body').removeAttr('style');
	});
	
	$('.menu-toggle').click(function () {
		$('.menu-wrapper').fadeIn();
		$('body').css('overflow', 'hidden');
		
	});
	
	$('.close-menu').click(function () {
		$('.menu-wrapper').fadeOut();
		$('body').removeAttr('style');
		
	});
});


$(document).keyup(function(e) {
		'use strict';
     if (e.keyCode === 27) { 
		$('.search-box').fadeOut();
		$('body').removeAttr('style');				   
    }
});